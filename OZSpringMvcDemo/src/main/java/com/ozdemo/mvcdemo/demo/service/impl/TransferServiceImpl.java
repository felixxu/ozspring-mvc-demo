package com.ozdemo.mvcdemo.demo.service.impl;

import com.ozdemo.mvcdemo.framework.annotation.Autowired;
import com.ozdemo.mvcdemo.framework.annotation.Service;
import com.ozdemo.mvcdemo.framework.annotation.Transactional;
import com.ozdemo.mvcdemo.demo.dao.AccountDao;
import com.ozdemo.mvcdemo.demo.pojo.Account;
import com.ozdemo.mvcdemo.demo.service.TransferService;

/**
 * 交易实现
 */
@Service
public class TransferServiceImpl implements TransferService {

    // 最佳状态
    // @Autowired 按照类型注入 ,如果按照类型无法唯一锁定对象，可以结合@Qualifier指定具体的id
    @Autowired
    private AccountDao accountDao;

    @Override
    @Transactional
    public void transfer(String fromCardNo, String toCardNo, int money) throws Exception {
        Account from = accountDao.queryAccountByCardNo(fromCardNo);
        Account to = accountDao.queryAccountByCardNo(toCardNo);

        from.setMoney(from.getMoney() - money);
        to.setMoney(to.getMoney() + money);

        accountDao.updateAccountByCardNo(to);
        System.out.println(1/0);
        accountDao.updateAccountByCardNo(from);
    }
}
