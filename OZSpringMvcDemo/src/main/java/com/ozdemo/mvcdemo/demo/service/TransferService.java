package com.ozdemo.mvcdemo.demo.service;

/**
 * 交易接口
 */
public interface TransferService {

    void transfer(String fromCardNo, String toCardNo, int money) throws Exception;
}
