package com.ozdemo.mvcdemo.demo.controller;

import com.ozdemo.mvcdemo.framework.annotation.Controller;
import com.ozdemo.mvcdemo.framework.annotation.RequestMapping;
import com.ozdemo.mvcdemo.framework.annotation.Security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/demo")
@Security({"user1","user2"})
public class DemoController {

    @RequestMapping("/demo1")
    @Security({"user3"})
    public void demo1(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        resp.getWriter().print("/demo/demo1 success!");
    }

    @RequestMapping("/demo2")
    @Security({"user4"})
    public void demo2(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        resp.getWriter().print("/demo/demo2 success!");
    }
}
