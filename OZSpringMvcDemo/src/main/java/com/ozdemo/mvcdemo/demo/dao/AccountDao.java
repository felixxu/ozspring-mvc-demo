package com.ozdemo.mvcdemo.demo.dao;

import com.ozdemo.mvcdemo.demo.pojo.Account;

/**
 * Account接口
 */
public interface AccountDao {

    Account queryAccountByCardNo(String cardNo) throws Exception;

    int updateAccountByCardNo(Account account) throws Exception;
}
