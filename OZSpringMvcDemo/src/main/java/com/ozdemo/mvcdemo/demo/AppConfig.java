package com.ozdemo.mvcdemo.demo;

import com.alibaba.druid.pool.DruidDataSource;
import com.ozdemo.mvcdemo.framework.annotation.Bean;
import com.ozdemo.mvcdemo.framework.annotation.ComponentScan;
import com.ozdemo.mvcdemo.framework.annotation.Value;

import javax.sql.DataSource;

/**
 * @Description: 配置类
 * @Author: Created by OrangeZh
 * @Date: Created in 2020/9/7 14:53
 */
@ComponentScan("com.ozdemo.mvcdemo")
public class AppConfig {

    @Value("${jdbc.driver}")
    private String driverClassName;
    @Value("${jdbc.url}")
    private String url;
    @Value("${jdbc.username}")
    private String username;
    @Value("${jdbc.password}")
    private String password;

    @Bean("dataSource")
    public DataSource createDataSource() {
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setDriverClassName(driverClassName);
        druidDataSource.setUrl(url);
        druidDataSource.setUsername(username);
        druidDataSource.setPassword(password);
        return druidDataSource;
    }
}
