package com.ozdemo.mvcdemo.demo.controller;

import com.ozdemo.mvcdemo.demo.pojo.Result;
import com.ozdemo.mvcdemo.demo.service.TransferService;
import com.ozdemo.mvcdemo.demo.utils.JsonUtils;
import com.ozdemo.mvcdemo.framework.annotation.Autowired;
import com.ozdemo.mvcdemo.framework.annotation.Controller;
import com.ozdemo.mvcdemo.framework.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("")
public class TransferController {

    @Autowired
    private TransferService transferService;

    /**
     * URL: /transfer
     * @param req
     * @param resp
     * @return
     */
    @RequestMapping("/transfer")
    public void transfer(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        // 设置请求体的字符编码
        req.setCharacterEncoding("UTF-8");

        String fromCardNo = req.getParameter("fromCardNo");
        String toCardNo = req.getParameter("toCardNo");
        String moneyStr = req.getParameter("money");
        int money = Integer.parseInt(moneyStr);

        Result result = new Result();

        try {

            // 2. 调用service层方法
            transferService.transfer(fromCardNo,toCardNo,money);
            result.setStatus("200");
        } catch (Exception e) {
            e.printStackTrace();
            result.setStatus("201");
            result.setMessage(e.toString());
        }

        // 响应
        resp.setContentType("application/json;charset=utf-8");
        resp.getWriter().print(JsonUtils.object2Json(result));
    }
}
