package com.ozdemo.mvcdemo.framework.annotation;

import java.lang.annotation.*;

/**
 * @Description: @Component注解
 * @Author: Created by OrangeZh
 * @Date: Created in 2020/9/4 17:15
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Component {
    String value() default "";
}