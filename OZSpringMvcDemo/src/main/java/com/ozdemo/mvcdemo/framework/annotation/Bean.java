package com.ozdemo.mvcdemo.framework.annotation;

import java.lang.annotation.*;

/**
 * @Description: @Bean注解
 * @Author: Created by OrangeZh
 * @Date: Created in 2020/9/4 17:28
 */
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Bean {
    String value() default "";

    String name() default "";

    boolean autowireCandidate() default true;

    String initMethod() default "";

    String destroyMethod() default "(inferred)";
}
