package com.ozdemo.mvcdemo.framework.annotation;

import java.lang.annotation.*;

/**
 * @Description: @Transactional注解
 * @Author: Created by OrangeZh
 * @Date: Created in 2020/9/4 17:37
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Transactional {
    String value() default "";

    String transactionManager() default "";
}
