package com.ozdemo.mvcdemo.framework.servlet;

import com.ozdemo.mvcdemo.framework.annotation.Controller;
import com.ozdemo.mvcdemo.framework.annotation.RequestMapping;
import com.ozdemo.mvcdemo.framework.annotation.Security;
import com.ozdemo.mvcdemo.framework.factory.BeanFactory;
import com.ozdemo.mvcdemo.framework.pojo.Handler;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@WebServlet(name="ozmvc",urlPatterns = "/*")
public class DispatcherServlet extends HttpServlet {

    // handlerMapping
    //存储url和Method之间的关联，就是Handler
    private List<Handler> handlerMapping = new ArrayList<>();

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        initHandlerMapping(servletConfig);
    }

    /**
     * 构造一个HandlerMapping处理器映射器
     * 最关键的环节
     * 目的：将url和method建立关联
     */
    private void initHandlerMapping(ServletConfig servletConfig) {
        //存储url和Method之间的映射关系
        Map<String, Object> beanMap = BeanFactory.getBeans();
        if (beanMap.isEmpty()) {
            return;
        }

        for (Map.Entry<String, Object> entry : beanMap.entrySet()) {
            // 获取ioc中当前遍历的对象的class类型
            Class<?> clazz = entry.getValue().getClass();

            if (!clazz.isAnnotationPresent(Controller.class)) {
                continue;
            }
            String baseUrl = "";
            if (clazz.isAnnotationPresent(RequestMapping.class)) {
                RequestMapping annotation = clazz.getAnnotation(RequestMapping.class);
                baseUrl = annotation.value(); // 等同于/demo
            }

            //处理@Security
            Set<String> securityUserCl = new HashSet<>();
            if (clazz.isAnnotationPresent(Security.class)) {
                Security annotation = clazz.getAnnotation(Security.class);
                String value[] = annotation.value();
                securityUserCl.addAll(new HashSet<>(Arrays.asList(value)));
            }

            // 获取方法
            Method[] methods = clazz.getMethods();
            for (int i = 0; i < methods.length; i++) {
                Method method = methods[i];

                //  方法没有标识LagouRequestMapping，就不处理
                if (!method.isAnnotationPresent(RequestMapping.class)) {
                    continue;
                }

                // 如果标识，就处理
                RequestMapping annotation = method.getAnnotation(RequestMapping.class);
                String methodUrl = annotation.value();  // /query
                String url = baseUrl + methodUrl;    // 计算出来的url /demo/query

                //处理@Security
                Set<String> securityUser = new HashSet<>();
                if (method.isAnnotationPresent(Security.class)) {
                    Security securityAnnotation = method.getAnnotation(Security.class);
                    String[] value = securityAnnotation.value();
                    securityUser.addAll(new HashSet<>(Arrays.asList(value)));
                    securityUser.addAll(securityUserCl);
                }

                // 把method所有信息及url封装为一个Handler
                Handler handler = new Handler(entry.getValue(), method, Pattern.compile(url), securityUser);

                // 计算方法的参数位置信息  // query(HttpServletRequest request, HttpServletResponse response,String name)
                Parameter[] parameters = method.getParameters();
                for (int j = 0; j < parameters.length; j++) {
                    Parameter parameter = parameters[j];

                    if (parameter.getType() == HttpServletRequest.class || parameter.getType() == HttpServletResponse.class) {
                        // 如果是request和response对象，那么参数名称写HttpServletRequest和HttpServletResponse
                        handler.getParamIndexMapping().put(parameter.getType().getSimpleName(), j);
                    } else {
                        handler.getParamIndexMapping().put(parameter.getName(), j);  // <name,2>
                    }

                }

                // 建立url和method之间的映射关系（map缓存起来）
                handlerMapping.add(handler);
            }
        }
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 根据uri获取到能够处理当前请求的hanlder（从handlermapping中（list））
        Handler handler = getHandler(req);

        if (handler!= null) {
            //判断是否允许访问
            if (handler.getSecurityUser() != null && handler.getSecurityUser().size() > 0) {
                String username = req.getParameter("username");
                if (!handler.getSecurityUser().contains(username)) {
                    resp.getWriter().write("Permission denied;");
                    return;
                }
            }

            // 参数绑定
            // 获取所有参数类型数组，这个数组的长度就是我们最后要传入的args数组的长度
            Class<?>[] parameterTypes = handler.getMethod().getParameterTypes();


            // 根据上述数组长度创建一个新的数组（参数数组，是要传入反射调用的）
            Object[] paraValues = new Object[parameterTypes.length];

            // 以下就是为了向参数数组中塞值，而且还得保证参数的顺序和方法中形参顺序一致

            Map<String, String[]> parameterMap = req.getParameterMap();

            // 遍历request中所有参数  （填充除了request，response之外的参数）
            for (Map.Entry<String, String[]> param : parameterMap.entrySet()) {
                // name=1&name=2   name [1,2]
                String value = StringUtils.join(param.getValue(), ",");  // 如同 1,2

                // 如果参数和方法中的参数匹配上了，填充数据
                if (!handler.getParamIndexMapping().containsKey(param.getKey())) {
                    continue;
                }

                // 方法形参确实有该参数，找到它的索引位置，对应的把参数值放入paraValues
                Integer index = handler.getParamIndexMapping().get(param.getKey());//name在第 2 个位置

                paraValues[index] = value;  // 把前台传递过来的参数值填充到对应的位置去
            }

            int requestIndex = handler.getParamIndexMapping().get(HttpServletRequest.class.getSimpleName()); // 0
            paraValues[requestIndex] = req;

            int responseIndex = handler.getParamIndexMapping().get(HttpServletResponse.class.getSimpleName()); // 1
            paraValues[responseIndex] = resp;

            // 最终调用handler的method属性
            try {
                handler.getMethod().invoke(handler.getController(), paraValues);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }

    }

    private Handler getHandler(HttpServletRequest req) {
        if (handlerMapping.isEmpty()) {
            return null;
        }
        String url = req.getRequestURI();
        for (Handler handler : handlerMapping) {
            Matcher matcher = handler.getPattern().matcher(url);
            if (!matcher.matches()) {
                continue;
            }
            return handler;
        }
        return null;

    }

}
