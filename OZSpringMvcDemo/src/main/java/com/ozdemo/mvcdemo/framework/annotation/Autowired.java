package com.ozdemo.mvcdemo.framework.annotation;

import java.lang.annotation.*;

/**
 * @Description: @Autowired注解
 * @Author: Created by OrangeZh
 * @Date: Created in 2020/9/4 17:29
 */
@Target({ElementType.CONSTRUCTOR, ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Autowired {
    boolean required() default true;
}
