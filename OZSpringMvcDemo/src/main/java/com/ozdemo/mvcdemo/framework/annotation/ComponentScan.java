package com.ozdemo.mvcdemo.framework.annotation;

import java.lang.annotation.*;

/**
 * @Description: @ComponentScan注解
 * @Author: Created by OrangeZh
 * @Date: Created in 2020/9/4 17:24
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
public @interface ComponentScan {
    String[] value() default {};

    String[] basePackages() default {};
}
