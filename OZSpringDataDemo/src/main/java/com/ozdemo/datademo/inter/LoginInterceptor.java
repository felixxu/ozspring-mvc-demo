package com.ozdemo.datademo.inter;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class LoginInterceptor extends HandlerInterceptorAdapter {
    public void setUncheckUrls(List<String> uncheckUrls) {
        this.uncheckUrls = uncheckUrls;
    }

    private List<String> uncheckUrls;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String URI = request.getRequestURI();
        for (String uri : uncheckUrls) {
            if (URI.contains(uri)) {
                return true;
            }
        }
        HttpSession session =  request.getSession();
        if(null == session) {
            return false;
        }
        Object loginUser = session.getAttribute("login");
        if (loginUser == null) {
            response.sendRedirect("/ozsss/login");
            return false;
        }
        return true;
    }
}
