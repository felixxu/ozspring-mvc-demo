package com.ozdemo.datademo.service.impl;


import com.ozdemo.datademo.dao.ResumeDao;
import com.ozdemo.datademo.pojo.Resume;
import com.ozdemo.datademo.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("resumeService")
public class ResumeServiceImpl implements ResumeService {
    @Autowired
    private ResumeDao resumeDao;

    @Override
    public List<Resume> findAll() {
        return resumeDao.findAll();
    }

    @Override
    public Resume findById(Long id) {
        return resumeDao.findById(id).get();
    }

    @Override
    public void delById(Long id) {
        resumeDao.deleteById(id);
    }

    @Override
    public void save(Resume resume) {
        resumeDao.save(resume);
    }
}
