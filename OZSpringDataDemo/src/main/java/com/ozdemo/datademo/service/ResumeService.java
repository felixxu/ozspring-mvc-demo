package com.ozdemo.datademo.service;

import com.ozdemo.datademo.pojo.Resume;

import java.util.List;

public interface ResumeService {

    public List<Resume> findAll();
    public Resume findById(Long id);
    public void delById(Long id);
    public void save(Resume resume);
}
