package com.ozdemo.datademo.controller;

import com.ozdemo.datademo.pojo.Result;
import com.ozdemo.datademo.utils.JsonUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Controller
public class LoginController {

    @RequestMapping("/login")
    public ModelAndView login(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        Map<String, Object> model=new HashMap<String, Object>();
        return new ModelAndView("login", "model", model);
    }

    @RequestMapping("/doLogin")
    public void doLogin(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        // 设置请求体的字符编码
        req.setCharacterEncoding("UTF-8");

        String username = req.getParameter("username");
        String password = req.getParameter("password");

        Result result = new Result();
        if(username.equals("admin") && password.equals("admin")){
            req.getSession().setAttribute("login", username);
            result.setStatus("200");
        }else {
            result.setStatus("201");
            result.setMessage("用户名或者密码错误");
        }
        // 响应
        resp.setContentType("application/json;charset=utf-8");
        resp.getWriter().print(JsonUtils.object2Json(result));
    }
}
