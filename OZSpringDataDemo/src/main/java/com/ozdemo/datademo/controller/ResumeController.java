package com.ozdemo.datademo.controller;


import com.ozdemo.datademo.pojo.Result;
import com.ozdemo.datademo.pojo.Resume;
import com.ozdemo.datademo.service.ResumeService;
import com.ozdemo.datademo.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/resume")
public class ResumeController {
    @Autowired
    private ResumeService resumeService;

    @RequestMapping("/list")
    public ModelAndView list(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        Map<String, Object> model = new HashMap<String, Object>();
        List<Resume> list = resumeService.findAll();
        model.put("data", list);
        model.put("test", "zxc");
        return new ModelAndView("list", "model", model);
    }

    @RequestMapping("/add")
    public ModelAndView add(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        Map<String, Object> model = new HashMap<String, Object>();
        return new ModelAndView("add", "model", model);
    }

    @RequestMapping("/update/{id}")
    public ModelAndView update(HttpServletRequest req, HttpServletResponse resp, @PathVariable("id") Long id) throws Exception {
        Map<String, Object> model = new HashMap<String, Object>();
        Resume resume = resumeService.findById(id);
        model.put("resume", resume);
        return new ModelAndView("add", "model", model);
    }

    @RequestMapping("/addOrUpdate")
    public void addOrUpdate(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        // 设置请求体的字符编码
        req.setCharacterEncoding("UTF-8");

        String idStr = req.getParameter("id");
        long id = 0;
        if (!idStr.equals("") && isNumeric(idStr)) {
            id = Long.parseLong(idStr);
        }
        String name = req.getParameter("name");
        String address = req.getParameter("address");
        String phone = req.getParameter("phone");

        Result result = new Result();
        Resume resume = new Resume();
        if (id > 0) {
            resume.setId(id);
        }
        resume.setName(name);
        resume.setAddress(address);
        resume.setPhone(phone);
        resumeService.save(resume);
        result.setStatus("200");

        // 响应
        resp.setContentType("application/json;charset=utf-8");
        resp.getWriter().print(JsonUtils.object2Json(result));
    }

    @RequestMapping("/delete/{id}")
    public void delete(HttpServletRequest req, HttpServletResponse resp, @PathVariable("id") Long id) throws Exception {
        // 设置请求体的字符编码
        req.setCharacterEncoding("UTF-8");
        Result result = new Result();
        Resume resume = resumeService.findById(id);
        if (resume == null) {
            result.setStatus("201");
            result.setMessage("不存在");
        } else {
            resumeService.delById(id);
            result.setStatus("200");
        }

        // 响应
        resp.setContentType("application/json;charset=utf-8");
        resp.getWriter().print(JsonUtils.object2Json(result));
    }

    private boolean isNumeric(String str) {
        for (int i = str.length(); --i >= 0; ) {
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }
}
