<%--
  Created by IntelliJ IDEA.
  User: zxcll
  Date: 2020/9/23
  Time: 3:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false" %>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>列 表</title>

<script type="text/javascript" src="../../js/jquery-3.4.1.min.js"></script>


<style type="text/css">
    body {
        background-color: #00b38a;
        text-align: center;
    }

    .lp-login {
        position: absolute;
        width: 500px;
        height: 300px;
        top: 50%;
        left: 50%;
        margin-top: -250px;
        margin-left: -250px;
        background: #fff;
        border-radius: 4px;
        box-shadow: 0 0 10px #12a591;
        padding: 57px 50px 35px;
        box-sizing: border-box
    }

    .lp-login .submitBtn {
        display: block;
        text-decoration: none;
        height: 48px;
        width: 150px;
        line-height: 48px;
        font-size: 16px;
        color: #fff;
        text-align: center;
        background-image: -webkit-gradient(linear, left top, right top, from(#09cb9d), to(#02b389));
        background-image: linear-gradient(90deg, #09cb9d, #02b389);
        border-radius: 3px
    }

    input[type='text'] {
        height: 30px;
        width: 250px;
    }

    span {
        font-style: normal;
        font-variant-ligatures: normal;
        font-variant-caps: normal;
        font-variant-numeric: normal;
        font-variant-east-asian: normal;
        font-weight: normal;
        font-stretch: normal;
        font-size: 14px;
        line-height: 22px;
        font-family: "Hiragino Sans GB", "Microsoft Yahei", SimSun, Arial, "Helvetica Neue", Helvetica;
    }

</style>
<script type="text/javascript">
    function del(id){
        $.ajax({
            url: '/ozsss//resume/delete'+id,
            type: 'POST',    //GET
            async: false,    //或false,是否异步
            timeout: 5000,    //超时时间
            dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
            success: function (data) {
                if ("200" == data.status) {
                    alert("删除成功~~~");
                    location.reload();
                } else {
                    alert("删除失败~~~,message:" + data.message);
                }
            }
        })
    }
    $(function () {
        $(".delBtn").bind("click", function () {

        })
    })
</script>
<body>

<div style="text-align: center;">
    <a href="/ozsss/resume/add">添 加</a>
</div>
<div style="text-align: center;">
    <table border="1" style="margin: auto;" width='60%'>
        <thead>
        <tr>
            <th>ID</th>
            <th>姓 名</th>
            <th>地 址</th>
            <th>电 话</th>
            <th>操 作</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${model.data}" var="resume" varStatus="var">
            <tr>
                <th>${resume.id}</th>
                <th>${resume.name}</th>
                <th>${resume.address}</th>
                <th>${resume.phone}</th>
                <th><a href="/ozsss/resume/update/${resume.id}">修 改</a> <a style="color: red" href="/ozsss//resume/delete/${resume.id}">删 除</a></th>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>
